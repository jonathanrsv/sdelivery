<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-inner">
    <div class="container-fluid">
        <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        </a>
        <a class="brand" href="#">SDelivery</a>
        <div class="nav-collapse collapse">
        <p class="navbar-text pull-right">
         <?php if (isset($_SESSION['usuarioNome']) ) {  ?>
         Logado como: <a href='#' class='navbar-link'><?php echo usuarioNome();?></a>                
         | <a href="<?php echo URL?>/admin/login/sair.php" class='navbar-link'>sair</a>
         <?php } ?>
        </p>
            <ul class="nav">
                <li><a href="#">Ajuda e Suporte</a></li>
                <li><a href="#sobre" data-toggle="modal">Sobre</a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
    </div>
</div>



<div class="modal hide fade" id="sobre" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Sobre o sDelivery</h3>
  </div>
  <div class="modal-body">
     <p>Projeto iniciado no dia 19/09/2012</p>
     <p>Versão: 1.0</p>
     <p>sDelivery 2012 todos os direitos reservados.</p>
  </div>
  <div class="modal-footer">
    <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Fechar</button>
  </div>
</div>