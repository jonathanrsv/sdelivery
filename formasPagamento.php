<?php
define(TITULO, "Formas de Pagamento - SDelivery");
require_once ("autoload.php");
require_once ("login/seguranca.php");
require ("header.php");
protegePagina();
?> 
<?php include("topo.php");?>
<div id="dash"></div>
<div class="container">
    <ul class="breadcrumb">
        <li><a href="painel.php">Início</a> <span class="divider">/</span></li>
        <li><a href="#">sistema</a> <span class="divider">/</span></li>
        <li class="active">Formas de Pagamento</li>  
    </ul>
<div class="row">
<?php include ("sidebar.php"); ?>        
<div class="span9">
     
     
        <ul class="nav nav-tabs" id="mytab">
            <li class="active"><a href="#categorias" data-toggle="tab">Formas de Pagamento</a></li>
            <li><a href="#addcategoria" data-toggle="tab">Adicionar Pagamento</a></li>
        </ul>
        <div class="tab-content">
            <?php echo trataMsg(); ?>
            <div class="tab-pane active" id="categorias">
                <legend>Formas de pagamento</legend>    
                 <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nome</th>
                            <th>Status</th>
                            <th style="text-align:center">Opções</th>
                        </tr>
                    </thead>                 
                    <tbody>
                        <?php
                        $sql = "SELECT * FROM tb_formaspag";
                        $query = $mysqli->query($sql);
                        while ($dados = $query->fetch_object()){
                        ?>
                        <tr>
                            <td><?php echo $dados->id_formapag ?></td>
                            <td><?php echo $dados->nome ?></td>
                            <td><?php if($dados->status == 1 ) { echo "Ativo"; } else { echo "Desativado";} ?></td>
                            <td style="text-align: center;">
                            <a href="#"><i class="icon-edit"></i></a>                                                         
                            </td>
                        </tr>
                     <?php } ?>
                     </tbody>    
               </table> 
            </div>
            <div class="tab-pane" id="addcategoria">
                <legend>Adicionar Pagamento</legend>
                       <form method="post" action="#">    
                    
                    <label>Nome </label>
                    <input type="text" name="nomepag" placeholder="Nome do pagamento"> 

                    <label>Status</label>
                    <select name="statuspag" class="span2">
                    <option value="1">Ativo</option>
                    <option value="0">Oculto</option>
                    </select>   

                    <label class="radio"></label>
                    <button type="submit" class="btn btn-primary" >Cadastrar</button>
                    
                    </div>
                    </div> 
                </form>    
            </div>

        </div>
    </div>
</div>
<?php
if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $nomepag = post(nomepag);
    $statuspag = post(statuspag);
      $sql = "INSERT INTO tb_formaspag (id_formapag, nome, status) VALUES (NULL, '$nomepag', '$statuspag')";
      $query = $mysqli->query($sql) OR trigger_error($mysqli->error, E_USER_ERROR);
       redireciona("admin/formasPagamento.php?msg=3");
}

?>
<?php include 'footer.php'?>