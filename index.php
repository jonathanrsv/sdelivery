<?php
define(TITULO, "SDelivery - Bem vindo");
require ("autoload.php");
require ("header.php");
session_start();
if(isset($_SESSION['usuarioNome'])){
     redireciona("admin/painel.php");
}//Verifica se o usuario está logado. Se sim, manda para painel.php. 
?> 

<?php include("topo.php");?>
<div class="container" style="width:650px">
    <legend>Seja Bem-vindo</legend>
    <div class="hero-unit">          
              <?php echo trataMsg(); ?>               
        <div class="form">
    <form class="form-horizontal" action="<?php echo URL."/admin/login/validar.php"; ?>" method="post">
    <div class="control-group">
        <label class="control-label" for="inputEmail">Usuario</label>
        <div class="controls">
        <input type="text" name="usuario" id="inputEmail" placeholder="Usuario">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inputPassword">Senha</label>
        <div class="controls">
        <input type="password" name="senha" id="inputPassword" placeholder="Senha">
        </div>
    </div>
    <div class="control-group">
        <div class="controls">
        <label class="checkbox">
            <input type="checkbox"> Lembre-se de mim
        </label>
        <button type="submit" class="btn">Entrar</button>
        </div>
    </div>
    </form>
        </div>
    </div>
</div>
<?php include 'footer.php'?>