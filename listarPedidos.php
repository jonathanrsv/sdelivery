<?php
define(TITULO, "Painel Inicial- SDelivery");
require_once ("autoload.php");
require_once ("login/seguranca.php");
protegePagina();
?>
        <div class="navbar">
            <div class="navbar-inner">
              <ul class="nav">
                  <form class="navbar-form pull-left">
                      <input type="text" id="pedido" name="pedido" class="span2" placeholder="Buscar Pedido"  autocomplete="off" data-provide="typeahead" data-items="4" 
                           data-source='[<?php
                $sql = "SELECT id_pedido FROM tb_pedidos";
                $query = $mysqli->query($sql);
                while($dados = $query->fetch_object()){                    
                    echo '"'.$dados->id_pedido.'",';
                }    ?>"teste"]'>
                   
                    
                    
                    <button type="submit" class="btn" >ok</button>
                  </form>
                </ul>    
            </div>
        </div>
   


        <table class="table table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Cliente</th>                  
                    <th>CPF</th> 
                    <th>Data Pedido</th>            
                    <th>Total</th>
                    <th>Status</th>
                   
                </tr>
            </thead>
            <tbody>
                <?php
                if(get(pedido)) {
                     $sql = "SELECT * FROM `tb_pedidos` INNER JOIN `tb_clientes` ON tb_pedidos.id_cliente = tb_clientes.id_cliente WHERE id_pedido LIKE '%".get(pedido)."%' ORDER BY data_pedido DESC";
                } else { 
                     $sql = "SELECT * FROM `tb_pedidos` INNER JOIN `tb_clientes` ON tb_pedidos.id_cliente = tb_clientes.id_cliente ORDER BY data_pedido DESC";
                }
                $query = $mysqli->query($sql);                
                    while($dados = $query->fetch_object()){
                    echo "<tr onclick=\"document.location = 'verPedido.php?id={$dados->id_pedido}';\" style=\"cursor:pointer;\">
                            <td>$dados->id_pedido</td>
                            <td>$dados->nome</td>                           
                            <td>$dados->cpf</td>
                            <td>".formataData($dados->data_pedido, BR, TRUE)."</td>                    
                            <th>".formataValor(totalCompraFrete($dados->id_pedido))."</th>
                            <td>".pedidoProcess($dados->id_pedido)."</td>
                       
                        </tr>";
                    }
                $mysqli->close();                 
                ?>
            </tbody>
           
</table>