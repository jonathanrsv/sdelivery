<?php
$_UP['pasta'] = 'uploads/';// Pasta onde o arquivo vai ser salvo
$_UP['tamanho'] = 1024 * 1024 * 2; // 2Mb Tamanho máximo do arquivo (em Bytes)
$_UP['extensoes'] = array('jpg', 'png', 'gif');  // Array com as extensões permitidas
$_UP['renomeia'] = false; // Renomeia o arquivo? (Se true, o arquivo será salvo como .jpg e um nome único)

// Array com os tipos de erros de upload do PHP
$_UP['erros'][0] = 'Não houve erro';
$_UP['erros'][1] = 'O arquivo no upload é maior do que o limite do PHP';
$_UP['erros'][2] = 'O arquivo ultrapassa o limite de tamanho especifiado no HTML';
$_UP['erros'][3] = 'O upload do arquivo foi feito parcialmente';
$_UP['erros'][4] = 'Não foi feito o upload do arquivo';



?>

