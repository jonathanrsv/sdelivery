<?php
define(TITULO, "Inserir Categoria - SDelivery");
require_once ("autoload.php");
require_once ("login/seguranca.php");
require ("header.php");
protegePagina();
?> 
<?php include("topo.php");?>

<div class="container">
    <ul class="breadcrumb">
        <li><a href="painel.php">Início</a> <span class="divider">/</span></li>
        <li><a href="#">Loja</a> <span class="divider">/</span></li>
        <li class="active">Inserir Categoria</li> 
    </ul>
<div class="row">
<?php include ("sidebar.php"); ?>        
<div class="span9">
    
    <div class="tabbable">
        <ul class="nav nav-tabs" id="mytab">
            <li class="active"><a href="#categorias" data-toggle="tab">Minhas Categorias</a></li>
            <li><a href="#addcategoria" data-toggle="tab">Adicionar Categoria</a></li>
        </ul>
        <div class="tab-content">
            <?php echo trataMsg(); ?>
            <div class="tab-pane active" id="categorias">
                <p>Categorias Cadastradas:</p>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nome</th>
                            <th>Status</th>
                            <th style="text-align:center">Opções</th>
                        </tr>
                    </thead>                 
                    <tbody>
                <?php
                $sql = "SELECT * FROM tb_categorias WHERE status = 1";
                $query = $mysqli->query($sql);
                while ($dados = $query->fetch_object()){                   
                ?>
                        <tr>
                            <td><?php echo $dados->id_categoria?></td>
                            <td><?php echo $dados->nome?></td>
                            <td>
                            <?php if($dados->status == 1){
                                echo '<i class="icon-ok" title="Ativa"></i>';
                            } else {
                                echo '<i class="icon-off" title="Oculta"></i>';
                                }                                
                             ?>
                            </td>
                            <td style="text-align:center">
                                <a href="#"><i class="icon-edit" title="Editar"></i></a>
                                <a href="?excluirCategoria=<?php echo $dados->id_categoria?>"><i class="icon-remove" title="Excluir"></i></a>
                            </td>
                        </tr>  
                <?php } ?>
                    </tbody>    
               </table>                
            </div>
            <div class="tab-pane" id="addcategoria">
                <legend>Inserir nova Categoria</legend>
                <form method="post" action="#">    
                    
                    <label>Nome </label>
                    <input type="text" name="nomecategoria" placeholder="Nome da categoria." value="<?php echo post(nomecategoria); ?>"> 

                    <label>Status</label>
                    <select name="statuscategoria" class="span2">
                    <option value="1">Ativo</option>
                    <option value="0">Oculto</option>
                    </select>   

                    <label class="radio"></label>
                    <button type="submit" class="btn btn-primary" >Cadastrar</button>
                    
                    </div>
                    </div> 
                </form>
        </div>
        </div>     
    </div> 
    </div>
    </div>
</div>
<?php
if($_SERVER['REQUEST_METHOD'] == 'POST') { 
    $nomeCategoria   = post(nomecategoria);
    $statusCategoria = post(statuscategoria);    
        if(empty($nomeCategoria)){
            redireciona("admin/gerenciarCategorias.php?msg=4");
        } 
        else {    
        $sql = "INSERT INTO tb_categorias (id_categoria, nome, status) VALUES (NULL, '$nomeCategoria', '$statusCategoria')";
        $query = $mysqli->query($sql) OR trigger_error($mysqli->error, E_USER_ERROR);
            redireciona("admin/gerenciarCategorias.php?msg=3");
    }
} 
if(get(excluirCategoria)){
    $id = get(excluirCategoria);
    $sql = "UPDATE `tb_categorias` SET `status` = '0' WHERE `tb_categorias`.`id_categoria` = '$id'";
    $query = $mysqli->query($sql) OR trigger_error($mysqli->error, E_USER_ERROR);
    redireciona("admin/gerenciarCategorias.php?msg=7");
}

?>
<?php include 'footer.php'?>