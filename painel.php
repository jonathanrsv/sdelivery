<?php
define(TITULO, "Painel Inicial- SDelivery");
require_once ("autoload.php");
require_once ("login/seguranca.php");
require ("header.php");
session_start();
protegePagina();
?>
    <?php if(!get(pedido)){ ?>
    <script type="text/javascript">
                            $(document).ready(function(){
                                    atualiza(); });                        
                            function atualiza(){                                
                                    $.get('listarPedidos.php', function(resultado){
                                    $('#span9').html(resultado); })
                                    setTimeout('atualiza()', 60000);
                            }
    </script>
    <?php }//O script só funciona se a buscar não for ativada  ?>
    
<?php include("topo.php");?>

    <div class="container">
            <ul class="breadcrumb">
                <li>Início</li>
            </ul>

        <div class="row">
            <?php include ("sidebar.php"); ?>        
            <div class="span9">
                <div id="span9">
                    <?php include 'listarPedidos.php';?>
                </div>
            </div>
            </div>
    </div>
<?php include 'footer.php'?>