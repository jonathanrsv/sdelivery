<?
// Inclui o arquivo com o sistema de segurança
require_once("../autoload.php");
require_once("seguranca.php");

// Verifica se um formulário foi enviado
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $usuario = post(usuario);
        $senha   = post(senha);
        // Utiliza uma função criada no seguranca.php pra validar os dados digitados
        if (validaUsuario($usuario, md5($senha)) == true) {
        // O usuário e a senha digitados foram validados, manda pra página interna
        header("Location:" .URL."/admin/painel.php");
        } else {
        // O usuário e/ou a senha são inválidos, manda de volta pro form de login
        expulsaVisitante();
        }
    }
?>