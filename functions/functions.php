<?php
function verificaCep($cepdestino){

        $origem = getOption(cep_cliente);

        $filename = 'http://maps.googleapis.com/maps/api/distancematrix/xml?origins='.$origem.'&destinations='.$cepdestino.'&sensor=false&language=PT';

        $xmlstr = file_get_contents($filename);    

        $dom = new domDocument();

        $dom->loadXML($xmlstr);

        $xml = simplexml_import_dom($dom);

        return $xml->row->element->distance->value;

}// Retorna a distancia em metros


function loadJs($nomeArquivo){

        $arquivo = "<script src='".URL."/admin/js/".$nomeArquivo.".js' type='text/javascript'> </script>\n";

        return $arquivo;

}//loadJs

function loadCss($nomeArquivo){

        $arquivo = "<link href='".URL."/admin/css/".$nomeArquivo.".css' rel='stylesheet'>\n";

        return $arquivo;

}//loadCss

function limpaTel($tel){

	$tel=str_replace(array("(", ")", "-"), "", $tel);

	return $tel;

}

function formataValor($moeda){    

	$moeda = number_format($moeda, 2, ',', '.');

	//$moeda = str_replace(".", ",", $moeda);	

	return "R$".$moeda;

}//formataValor



function geraId(){

        $diames = date('dm'); 

        $cod = rand(1001,9999); //gera um codigo aleatorio de 4 digitos        

        $gerador = "$diames"."$cod";

        return $gerador;

}//geraId

function ArrayCarrinho()

{

	if (!isset($_SESSION['carrinho']) || !is_array($_SESSION['carrinho']) || (count ($_SESSION['carrinho']) == 0))

	    return array();

	    

	$cods = array();

	$carro = isset($_SESSION['carrinho']) ? $_SESSION['carrinho'] : array();

	foreach ($carro as $k => $v)

    {

	    $cods[$k] = $v['id'];

    }

    return $cods;

}




function anti_injection($sql, $formUse = true){

        // remove palavras que contenham sintaxe sql

        $sql = preg_replace("/(from|select|insert|delete|where|drop table|show tables|#|\*|--|\\\\)/i","",$sql);    

        //$sql = strip_tags($sql);//tira tags html e php

        if(!$formUse || !get_magic_quotes_gpc())

            $sql = addslashes($sql);//Adiciona barras invertidas a uma string

        return $sql;

}//anti_injection



function get($parametro = NULL){

	$get = anti_injection($_GET[$parametro], false);	

	return $get;

}//get



function post($parametro = NULL){

	$post = anti_injection($_POST[$parametro]);	

	return $post;

}//post



function redireciona($url){

        header('location: '.URL.'/'.$url);

}//redireciona



function formataData($data = NULL, $tipo = 'BR', $datetime = false){

	if(!empty($data)){

		if($tipo == 'BR'){

			$dt = explode(" ",$data);

   			$data = explode("-",$dt[0]);

			

			if($datetime){

				$data = $data[2].'/'.$data[1].'/'.$data[0].' '.$dt[1];

			}

			else{

				$data = $data[2].'/'.$data[1].'/'.$data[0];

			}

		}

		else{

			$dt = explode(" ",$data);

			$data = explode("/",$dt[0]);

			if($datetime){

        		$data = $data[2].'-'.$data[1].'-'.$data[0].' '.$dt[1];

			}

			else{

				$data = $data[2].'-'.$data[1].'-'.$data[0];

			}

		}

	}

	else{

		return false;

	}

	

	return $data;

}//formatadata

function datasql($databr) {

	if (!empty($databr)){

	$p_dt = explode('/',$databr);

	$data_sql = $p_dt[2].'-'.$p_dt[1].'-'.$p_dt[0];

	return $data_sql;

	}

}



function trataMsg(){

   if(isset($_GET['msg'])){ 

    switch(get(msg)) {

        case 1:

            $tipoerro = "alert-error"; //alert-error | alert-success

            $mensagem = "Login ou senha incorretos, Tente novamente.";

            break;

        case 2:

            $tipoerro = "alert-error"; //alert-error | alert-success

            $mensagem = "É necessario estar logado para visualizar o sistema.";

            break;    

        case 2:

            $tipoerro = "alert-error"; //alert-error | alert-success

            $mensagem = "Você não pode fazer isso.";

            break; 

        case 3:

            $tipoerro = "alert-success"; //alert-error | alert-success

            $mensagem = "Dados cadastrados com sucesso.";

            break; 

        case 4:

            $tipoerro = "alert-error"; //alert-error | alert-success

            $mensagem = "Atenção! Preencha todos os campos para finalizar.";

            break; 

        case 5:

            $tipoerro = "alert-success"; //alert-error | alert-success

            $mensagem = "Suas configurações foram salvas com sucesso.";

            break; 

        case 6:

            $tipoerro = "alert-error"; //alert-error | alert-success

            $mensagem = "Não foi possivel fazer o upload da imagem. Tente Novamente";

            break;

        case 7:

            $tipoerro = "alert-success"; //alert-error | alert-success

            $mensagem = "Dados Alterados com sucesso.";

            break;

        case 8:

            $tipoerro = "alert-success"; //alert-error | alert-success

            $mensagem = "Produto excluido.";

            break;

        default :

            $mensagem = "Erro não definido no sistema";

    } 



    $conteudo =  '<div class="alert ' . $tipoerro.'">

                  '.$mensagem.'

                  <button type="button" class="close" data-dismiss="alert">×</button>

                  </div>';



   }

    return $conteudo;

  

}//trata erro

function usuarioNome(){

        $nome = $_SESSION["usuarioNome"];

        return $nome;    

}



function pedidoProcess($id){

        $mysqli = new mysqli(SERVIDOR, USUARIO, SENHA, DB); 

        $sql = "SELECT * FROM tb_pedidos WHERE id_pedido = '$id'";

        $query = $mysqli->query($sql);

        $dados = $query->fetch_object();

        switch($dados->status_pedido){

            case 1:

                $status = '<span class="label label-success">Finalizado</span>';

                break;

            case 2:

                $status = '<span class="label label-warning">Processando</span>';

                break;

            case 3:

                $status = '<span class="label label-info">Saiu para entrega</span>';

                break;

            case 4:

                $status = '<span class="label label-important">Cancelado</span>';

                break;

            default:

                $status = '<span class="label label-inverse">ERRO!</span>';

                break;

    }

    return $status;

}



function statusPedido($id){

        $mysqli = new mysqli(SERVIDOR, USUARIO, SENHA, DB); 

        $sql = "SELECT * FROM tb_pedidos WHERE id_pedido = '$id'";

        $query = $mysqli->query($sql);

        $dados = $query->fetch_object();

        return $dados->status_pedido;

}//statusPedido



function getOption($option_nome){

        //Pega o valor da opção no banco e exibe

        $mysqli = new mysqli(SERVIDOR, USUARIO, SENHA, DB);      

        $sql = "SELECT * FROM tb_opcoes WHERE option_nome = '$option_nome'";

        $query = $mysqli->query($sql);

        $dados = $query->fetch_object();

        return $dados->option_valor;      

        

}//getOption



function totalCompra($idpedido){

        //Pega o valor total da compra atraves do ID do pedido e exibe

        $mysqli = new mysqli(SERVIDOR, USUARIO, SENHA, DB);      

        $sql = "SELECT SUM((valor_unitario*qtd)-desconto_unitario) as soma FROM tb_pedidos_itens where id_pedido='$idpedido'";

        $query = $mysqli->query($sql);

        $dados = $query->fetch_object();

        $total = $dados->soma;

        return $total;

}//totalCompra



function totalCompraFrete($idpedido){

        //Soma o valor total da compra + frete e exibe

        $frete = getOption(frete);

    

        $mysqli = new mysqli(SERVIDOR, USUARIO, SENHA, DB);      

        $sql = "SELECT SUM((valor_unitario*qtd)-desconto_unitario) as soma FROM tb_pedidos_itens where id_pedido='$idpedido'";

        $query = $mysqli->query($sql);

        $dados = $query->fetch_object();

        $total = $dados->soma+$frete;

        return $total;

}//totalCompraFrete



function alterarOption($option_nome, $novovalor){

        //altera o valor da opção.

        $mysqli = new mysqli(SERVIDOR, USUARIO, SENHA, DB);

        $sql = "UPDATE `tb_opcoes` SET `option_valor` = '$novovalor' WHERE `tb_opcoes`.`option_nome` = '$option_nome'";

        $query = $mysqli->query($sql) OR trigger_error($mysqli->error, E_USER_ERROR);

        echo "sucesso";

}//alterarOption



function idEndereco($idpedido){

         //Pega o id do endereço atraves do id do pedido (para consultas)

         $mysqli = new mysqli(SERVIDOR, USUARIO, SENHA, DB);

         $sql = "SELECT id_endereco FROM tb_pedidos WHERE id_pedido = ".get(id)."";

         $query = $mysqli->query($sql);

         $dados = $query->fetch_object();

         return $dados->id_endereco;

         

}

function obterTelefone($idpedido){

        $mysqli = new mysqli(SERVIDOR, USUARIO, SENHA, DB);

        $sql = "SELECT tb_pedidos.id_telefone, tb_pedidos.id_pedido, tb_telefone.telefone FROM tb_pedidos INNER JOIN tb_telefone ON tb_pedidos.id_telefone = tb_telefone.id_telefone WHERE tb_pedidos.id_pedido = ".$idpedido."";

        $query = $mysqli->query($sql);

        $dados = $query->fetch_object(); 

        return $dados->telefone;



}//obter o telefone com base no id do pedido







?>