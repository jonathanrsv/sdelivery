<?php
define(TITULO, "Configurações - SDelivery");
require_once ("autoload.php");
require_once ("login/seguranca.php");
require ("header.php");
protegePagina();
?> 
<?php include("topo.php");?>
<div class="container">
    <ul class="breadcrumb">
        <li><a href="painel.php">Início</a> <span class="divider">/</span></li>
        <li><a href="#">sistema</a> <span class="divider">/</span></li>
        <li class="active">configurações</li>  
    </ul>
<div class="row">
<?php include ("sidebar.php"); ?>        
<div class="span9">
<legend>Configurações da loja</legend>

<form method="post" action="" class="form-horizontal">    
    <?php echo trataMsg(); ?>
    <div class="control-group">
            <label class="control-label">Nome Loja</label>
            <div class="controls">
            <input type="text" name="nomeloja" value="<?php echo getOption(nome_loja); ?>"> 
    </div>
    </div>
    
    <div class="control-group">
            <label class="control-label">Descrição </label>
            <div class="controls">
            <textarea rows="3" name="descricao" style="width:400px;"><?php echo getOption(descricao_loja);?></textarea> 

    </div>
    </div>
    
    <div class="control-group">
            <label class="control-label">Email de Contato</label>
            <div class="controls">
            <input type="text" name="emailcontato" value="<?php echo getOption(email_contato); ?>"> 
    </div>
    </div>
    
    <div class="control-group">
            <label class="control-label">Telefone de Contato</label>
            <div class="controls">
            <input type="text" name="telcontato" id="telcontato" value="<?php echo getOption(telefone_contato); ?>"> 
    </div>
    </div>
    
    <div class="control-group">
        <label class="control-label">Frete</label>
        <div class="controls">
        <input type="text" name="frete" id="frete"value="<?php echo formataValor(getOption(frete)); ?>"> 
    </div>
    </div>
    
    <div class="control-group">
            <label class="control-label">Limite de Entrega</label>
            <div class="controls">
            <input type="text" name="limiteentrega" class="input-mini" value="<?php echo getOption(limite_entrega); ?>" style="text-align: right;"> 
            <a href="#" id="popoverEntrega" rel="popover" data-content="Define a distância maxima em metros o limite de entrega (1km = 1000 metros)" data-original-title="Limite de Entrega"><i class="icon-question-sign"></i></a>  
            </div>
    </div>
    
    <div class="control-group">
            <label class="control-label">Horário de Funcionamento</label>
            <div class="controls">
            <input type="text" name="horainicial" id="horainicial" class="input-mini" value="<?php echo getOption(hora_inicial); ?>" style="text-align: center;"> até 
            <input type="text" name="horafinal" id="horafinal" class="input-mini" value="<?php echo getOption(hora_final); ?>" style="text-align: center;">
            <a href="#" id="popoverHorario" rel="popover" data-content="Define o horario de funcionamento do sistema" data-original-title="Horario de Funcionamento"><i class="icon-question-sign"></i></a>
            </div>
    </div>
    
    <div class="control-group">
        <label class="control-label">Cep Loja</label>
        <div class="controls">
        <input type="text" name="ceploja" id="ceploja" value="<?php echo getOption(cep_cliente); ?>"> 
    </div>
    </div>
    
    <div class="control-group">
    <div class="controls">

    <button type="submit" class="btn btn-primary">Salvar Alterações</button>
    </div>
    </div>
    
</form>
    </div>
</div>


<?php
if($_SERVER['REQUEST_METHOD'] == 'POST') { 
    $nomeloja       = post(nomeloja);
    $descricao      = post(descricao);
    $emailcontato   = post(emailcontato);
    $telcontato     = post(telcontato);
    $limiteentrega  = post(limiteentrega);
    $horainicial    = post(horainicial);
    $horafinal      = post(horafinal);
    $ceploja        = post(ceploja);  
    $frete          = post(frete);
        alterarOption('limite_entrega',     $limiteentrega);
        alterarOption('cep_cliente',        $ceploja);  
        alterarOption('nome_loja',          $nomeloja);
        alterarOption('email_contato',      $emailcontato);
        alterarOption('telefone_contato',   $telcontato);
        alterarOption('descricao_loja',     $descricao);
        alterarOption('hora_inicial',       $horainicial);
        alterarOption('hora_final',         $horafinal);
        alterarOption('frete',              $frete);
    redireciona('admin/configuracoes.php?msg=5');
}//Fim Request_method
?>
</div>
<?php include 'footer.php'?>
