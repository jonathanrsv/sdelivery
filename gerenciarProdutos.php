<?php
define(TITULO, "Inserir Produto - SDelivery");
require_once ("autoload.php");
require_once ("login/seguranca.php");
require ("header.php");
protegePagina();
?> 
<?php include("topo.php");?>
<div class="container">
    <ul class="breadcrumb">
        <li><a href="painel.php">Início</a> <span class="divider">/</span></li>
        <li><a href="#">Loja</a> <span class="divider">/</span></li>
        <li class="active">Inserir Produtos</li> 
    </ul>
<div class="row">
<?php include ("sidebar.php"); ?>   
<div class="span9">
    
 <div class="tabbable"> <!-- Only required for left/right tabs -->
  <ul class="nav nav-tabs">    
    <li class="active"><a href="#tab2" data-toggle="tab">Meus Produtos</a></li>
    <li><a href="#tab1" data-toggle="tab">Adicionar Produto</a></li>
  </ul>
  <div class="tab-content">
       <?php echo trataMsg(); ?>
  <div class="tab-pane" id="tab1">
   <legend>Inserir novo produto</legend>
    <form method="post" action=" " class="form-horizontal" enctype="multipart/form-data">    
       
        <div class="control-group">
        <label class="control-label">Nome Produto</label>
        <div class="controls">
        <input type="text" name="nomeproduto" placeholder="Nome do produto." value="<?php echo post(nomeproduto); ?>"> 
        </div>
        </div>
        
        <div class="control-group">
        <label class="control-label">Seleciona uma Categoria</label>
        <div class="controls">
        <select name="categoria" class="span2">
        <option></option>
        <?php
        $sql = "SELECT * FROM tb_categorias ORDER BY nome ASC";
        $query = $mysqli->query($sql);
        while ($dados = $query->fetch_object()){
        echo "
        <option value='$dados->id_categoria'>$dados->nome</option>        
        ";
         }
        ?>
        </select>   
        </div>
        </div>
        
        <div class="control-group">
        <label class="control-label">Descrição</label>
        <div class="controls">
        <textarea rows="3" name="descricao" style="width:400px;" placeholder="Breve descrição do produto."><?php echo post(descricao);?></textarea> 
        </div>
        </div>
        
        <div class="control-group">
        <label class="control-label">Preço</label>
        <div class="controls">
        <input type="text" name="preco" class="input-mini" placeholder="R$" value="<?php echo post(preco); ?>">  
        </div>
        </div>
        
        <div class="control-group"> 
        <label class="control-label">Status</label>
        <div class="controls">
        <select name="statusproduto" class="span2">
        <option value="1">Ativo</option>
        <option value="0">Oculto</option>
        </select>   
        </div>
        </div>
        
        <div class="control-group"> 
        <label class="control-label">Imagem</label>
        <div class="controls">
        <input type="file" name="arquivo" />                 
        </div>
        </div>
        
        <div class="control-group">
            <div class="controls">
             
            <button type="submit" class="btn btn-primary">Cadastrar</button>
            </div>
        </div>
        </form>

    </div>
    <div class="tab-pane  active" id="tab2">
      <p>Olá, estou na seção 2</p>
        <ul class="thumbnails">
       <?php
        $sql = "SELECT * FROM tb_produtos WHERE status = 1";
        $query = $mysqli->query($sql);
        while ($dados = $query->fetch_object()){
        ?>         
            <li class="span3">
                <div class="thumbnail">
                   
                    
                <img src="<?php echo $dados->imagem ?>" width="300" height="200" alt="" >
                <h3><?php echo $dados->nome ?></h3>
                <p><?php echo $dados->descricao ?></p>  
                <p><strong>Preço:</strong> <?php echo formataValor($dados->valor) ?></p>  
                <p><a href="#" class="btn btn-primary btn-small">Editar</a>
                <a href="?excluirProduto=<?php echo $dados->id_produto ?>" class="btn btn-danger btn-small" >Excluir</a>
                </p>
                </div>
            </li>           
      
      <?php } ?>
        </ul>
    </div>
  </div>
</div>
    

            
        </div>
        </div> 
    
        </div>
    </div>
</div>
<?php

if($_SERVER['REQUEST_METHOD'] == 'POST') { 
    $nomeproduto    = post(nomeproduto);
    $categoria      = post(categoria);
    $descricao      = post(descricao);
    $preco          = post(preco);
    $status         = post(statusproduto);
    
        // Verifica se houve algum erro com o upload. Se sim, exibe a mensagem do erro
    if ($_FILES['arquivo']['error'] != 0) {
        redireciona("admin/gerenciarProdutos.php?msg=6");
    exit; // Para a execução do script
    }
    $extensao = strtolower(end(explode('.', $_FILES['arquivo']['name'])));
    if (array_search($extensao, $_UP['extensoes']) === false) {
    echo "Por favor, envie arquivos com as seguintes extensões: jpg, png ou gif";
    }

    else if ($_UP['tamanho'] < $_FILES['arquivo']['size']) {
    echo "O arquivo enviado é muito grande, envie arquivos de até 2Mb.";
    }

    else {

    if ($_UP['renomeia'] == true) {

    $nome_final = time().'.jpg';
    } else {

    $nome_final = $_FILES['arquivo']['name'];
    
    }

    if (move_uploaded_file($_FILES['arquivo']['tmp_name'], $_UP['pasta'] . $nome_final)) {
    $caminhoimagem = "".$_UP['pasta'] . $nome_final."";
    
    $OBImagem = new OBImagem();
    $OBImagem->redimensiona_fixo($caminhoimagem, $caminhoimagem,300,200,100);
    
    echo "Upload efetuado com sucesso!";
    echo '<br /><a href="' . $_UP['pasta'] . $nome_final . '">Clique aqui para acessar o arquivo</a>';
    } else {
    echo "Não foi possível enviar o arquivo, tente novamente";
    } 
   } 
   $sql   = "INSERT INTO `tb_produtos` (`id_produto`, `id_categoria`, `nome`, `descricao`, `valor`, `qtd_estoque`, `status`, `imagem`) 
           VALUES (NULL, '$categoria', '$nomeproduto', '$descricao', '$preco', '0', '$status', '$caminhoimagem');";
   $query = $mysqli->query($sql) OR trigger_error($mysqli->error, E_USER_ERROR);
   redireciona("admin/gerenciarProdutos.php?msg=3");
   
   
}//Fim inserir novo Produto

if(get(excluirProduto)) {
    $id = get(excluirProduto);
    $sql = "UPDATE `tb_produtos` SET `status` = '0' WHERE `tb_produtos`.`id_produto` = '$id'";
    $query = $mysqli->query($sql) OR trigger_error($mysqli->error, E_USER_ERROR);
    redireciona("admin/gerenciarProdutos.php?msg=7");
}

?>
<?php include 'footer.php'?>












