<?php
define(TITULO, "Ver Pedido - SDelivery");
require_once ("autoload.php");
require_once ("login/seguranca.php");
require ("header.php");
protegePagina();
if(get(id) == ''){        
    redireciona("admin/painel.php");
    }// Verifica se algum pedido foi selecionado se falso redireciona para o painel
?> 

<div class="container">
    <ul class="breadcrumb">
        <li><a href="painel.php">Início</a> <span class="divider">/</span></li>
        <li><a href="<?php echo URL."/admin/painel.php" ?>">Ultimos Pedidos</a> <span class="divider">/</span></li>
        <li class="active">Ver Pedido</li> 
    </ul>
<div class="row">
<?php include ("sidebar.php"); ?>   
<div class="span4">     

    <div class="infopedido">
            <div class="arrow"></div>
            <h3 class="popover-title">Informações do Pedido</h3>
            <div class="popover-content">
            <table class="table">
                <?php
                $sql = "SELECT * FROM tb_pedidos WHERE id_pedido = ".get(id)."";
                $query = $mysqli->query($sql);
                
                   while ($dados = $query->fetch_object()){ 
                ?>
                <tbody>
                <tr>
                    <td>Codigo do Pedido</td>
                    <td><strong><?php echo $dados->id_pedido ?></strong></td>
                </tr>
                <tr>
                    <td>Data</td>
                    <td><strong><?php echo formataData($dados->data_pedido, 'BR', TRUE) ?></strong></td>
                </tr>
                <tr>
                    <td>Status</td>
                    <td>
                    <div class="dropdown">                       
                        <a class="dropdown-toggle" id="dLabel" role="button" style="text-decoration: none;" data-toggle="dropdown" data-target="#" href="#">
                        <?php 
                           echo pedidoProcess(get(id));
                        ?>                      
                            
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                                <?php
                                    switch (statusPedido(get(id))) {
                                                case 1:
                                                    $status = '<li><a tabindex="-1" href="?id='.$dados->id_pedido.'&statusPedido=2">Processando</a></li>
                                                               <li><a tabindex="-1" href="?id='.$dados->id_pedido.'&statusPedido=4">Cancelado</a></li>
                                                               <li><a tabindex="-1" href="?id='.$dados->id_pedido.'&statusPedido=3">Saiu para entrega</a></li>';                                          
                                                    break;
                                                
                                                case 2:
                                                    $status = '<li><a tabindex="-1" href="?id='.$dados->id_pedido.'&statusPedido=4">Cancelado</a></li>
                                                               <li><a tabindex="-1" href="?id='.$dados->id_pedido.'&statusPedido=1">Finalizado</a></li>
                                                               <li><a tabindex="-1" href="?id='.$dados->id_pedido.'&statusPedido=3">Saiu para entrega</a></li>';
                                                    break;
                                                
                                                case 3:
                                                    $status = '<li><a tabindex="-1" href="?id='.$dados->id_pedido.'&statusPedido=2">Processando</a></li>
                                                               <li><a tabindex="-1" href="?id='.$dados->id_pedido.'&statusPedido=4">Cancelado</a></li>
                                                               <li><a tabindex="-1" href="?id='.$dados->id_pedido.'&statusPedido=1">Finalizado</a></li>';
                                                    break;
                                                
                                                case 4:
                                                    $status = '<li><a tabindex="-1" href="?id='.$dados->id_pedido.'&statusPedido=2">Processando</a></li>
                                                               <li><a tabindex="-1" href="?id='.$dados->id_pedido.'&statusPedido=3">Saiu para entrega</a></li>
                                                               <li><a tabindex="-1" href="?id='.$dados->id_pedido.'&statusPedido=1">Finalizado</a></li>';
                                                    break;
                                                
                                                default:
                                                    $status = "erro";
                                                    break;
                                            }
                                            echo $status;
                                ?> 
                            
                    
                        </ul>                        
                    </div>                    
                    </td>
                </tr>               
                </tbody>
              <?php } ?>
            </table>
            </div>
          </div>
   
</div>
<div class="span4">     
      <div class="infopedido">
            <div class="arrow"></div>
            <h3 class="popover-title">Dados do cliente</h3>
            <div class="popover-content">
            <table class="table">
                <tbody>
                <?php 
                $sql = "SELECT id_cliente FROM tb_pedidos WHERE id_pedido = ".get(id)."";
                $query = $mysqli->query($sql);
                $dados = $query->fetch_object();
                
                $infocliente = "SELECT * FROM tb_clientes WHERE id_cliente = $dados->id_cliente";
                $query2 = $mysqli->query($infocliente);
                while ($cliente = $query2->fetch_object()){
     
                ?>
                  
                <tr>
                    <td>Nome do Cliente</td>
                    <td><strong><?php echo $cliente->nome ?></strong></td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td><strong><?php echo $cliente->email ?></strong></td>
                </tr>
                <tr>
                    <td>Telefone de Contato</td>
                    <td><?php echo obterTelefone(get(id)); ?></td>
                </tr>
               <?php } ?> 
                </tbody>
            </table>           
            </div>
          </div>
</div>
       <div class="espaco"></div>
    <div class="span4">
        <div class="infopedido">
            <div class="arrow"></div>
            <h3 class="popover-title">Endereço de Entrega</h3>
            <div class="popover-content">
            <?php 
            $sql = "SELECT * FROM tb_enderecos WHERE id_endereco = ".idEndereco(get(id))."";
            $query = $mysqli->query($sql);
             while ($dados = $query->fetch_object()){
            ?> 
                <?php echo "$dados->endereco, $dados->numero - $dados->complemento <br> $dados->cidade - $dados->cep"; ?>
                
            <?php } ?><br>
               
            </div>
          </div>
    </div>
       
           <div class="span4">
        <div class="infopedido">
            <div class="arrow"></div>
            <h3 class="popover-title">Forma de Pagamento</h3>
            <div class="popover-content">
            <?php 
            $sql = "SELECT nome FROM tb_pedidos INNER JOIN tb_formaspag ON tb_pedidos.id_formapag = tb_formaspag.id_formapag WHERE tb_pedidos.id_pedido = ".get(id)."";
            
            $query = $mysqli->query($sql);
             while ($dados = $query->fetch_object()){
            ?> 
                <?php echo $dados->nome ?>
                
            <?php } ?>
            </div>
          </div>
    </div>
      
    <div class="espaco"></div>
    <div class="span9">     
      <div class="infopedido">
            <div class="arrow"></div>
            <h3 class="popover-title">Itens do Pedido</h3>
            <div class="popover-content">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Produto</th>
                            <th>Preço</th>
                            <th style="text-align: center;">Qtd</th> 
                            <th style="text-align: center;">Sub-Total</th>            
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sql = "SELECT * FROM tb_pedidos_itens INNER JOIN tb_produtos ON tb_pedidos_itens.id_produto = tb_produtos.id_produto WHERE tb_pedidos_itens.id_pedido = ".get(id)."";                      
                        $query = $mysqli->query($sql);
                         while($dados = $query->fetch_object()){                             
                        ?>
                        <tr>
                            <td><a href="#"><?php echo $dados->nome ?></a></td>
                            <td><?php echo formataValor($dados->valor) ?></td>
                            <td style="text-align: center;"><?php echo $dados->qtd ?>  </td>
                            <td style="text-align: center;"><strong><?php echo formataValor($dados->valor_unitario*$dados->qtd) ?></strong></td>                                   
                        </tr> 
                       <?php } ?>
                    </tbody>                        
                </table>  
              
                <p>Total do Pedido: <?php echo formataValor(totalCompra(get(id))) ?></p>
                <p>Frete: <?php echo formataValor(getOption(frete));?></p>
                <p>Valor Total: <strong><?php echo formataValor(totalCompraFrete(get(id))) ?></strong></p>
                
            </div>
          </div>
        
</div>
    
  
    
 </div> 
</div>
<?php include("topo.php");?>
<?php include("footer.php")?>
<?php
if(get(statusPedido)){
    redireciona("admin/verPedido.php?id=".get(id)."");
    $mysqli = new mysqli(SERVIDOR, USUARIO, SENHA, DB);
    $sql = "UPDATE `tb_pedidos` SET `status_pedido` = '".get(statusPedido)."' WHERE `id_pedido` = '".get(id)."'";
    $query = $mysqli->query($sql) OR trigger_error($mysqli->error, E_USER_ERROR);
}// Altera o status do pedido e redireciona para a mesma pagina do pedido


?>